package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		String userMessage = request.getParameter("id");
		Message message = null;

		if (userMessage.matches("^[0-9]*$")){
			message = new MessageService().editSelect(userMessage);
		}
		if (message == null) {
			session.setAttribute("errorMessages", "不正なパラメータが入力されました");
			response.sendRedirect("./");
			return;
		}

		request.setAttribute("message", message);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);

	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();

        String editText = request.getParameter("text");

        Message messages = getMessage(request);
        if (!isValid(editText, errorMessages)) {
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("message", messages);
            request.getRequestDispatcher("/edit.jsp").forward(request, response);
            return;
        }

        new MessageService().update(messages);
        response.sendRedirect("./");
    }

    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

        Message message = new Message();
        message.setId(Integer.parseInt(request.getParameter("id")));
        message.setText(request.getParameter("text"));
        return message;
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
