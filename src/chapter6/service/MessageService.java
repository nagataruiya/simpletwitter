package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			String startTime = null;
			if(!StringUtils.isEmpty(start)) {
				startTime = start + " 00:00:00";
			} else {
				startTime = "2020/01/01 00:00:00";
			}
			String endTime = null;
			if(!StringUtils.isEmpty(end)) {
				endTime = end + " 23:59:59";
			} else {
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
				endTime = sdf.format(date);
			}
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startTime, endTime);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(String id) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message editSelect(String id) {
		Connection connection = null;
		try {
			connection = getConnection();
			Message edit = new MessageDao().editSelect(connection, id);
			commit(connection);
			return edit;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message messages) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().update(connection, messages);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}


	}
}
